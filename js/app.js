(function() {
  'use strict';

  angular
  .module('module', ['ui.router'])

  .config(['$stateProvider', '$urlRouterProvider', '$httpProvider', function($stateProvider, $urlRouterProvider, $httpProvider){

    $httpProvider.defaults.xsrfCookieName = 'csrftoken';
    $httpProvider.defaults.xsrfHeaderName = 'X-CSRFToken';


    $stateProvider
    .state('home', {
      url: '/',
      templateUrl: 'state/state.html',
      controller: 'controller',
      controllerAs: 'vm'
    })
  }])

})();
