(function() {
    'use strict';

    angular
        .module('module')
        .controller('controller', controller);

    controller.$inject = ['$scope','service'];

    /* @ngInject */
    function controller($scope, service) {
        var vm = this;

        $scope.login = login;
        $scope.peticion = peticion;

        function login(){
          service.login($scope.username, $scope.password)
          .then(function(response){
            console.log(response);
            $scope.responseLogin = response;
          }, function(err){
            console.log(err);
          });
          console.log($scope.username+'/ /'+ $scope.password);
        };

        function peticion(){
          service.peticion($scope.url, $scope.token).then(function(response){
            $scope.response = response;
            console.log(response);
          }, function(err){
            console.log(err);
          })
        }

        activate();

        function activate() {
          console.log("activated controller");
        }
    }

})();
