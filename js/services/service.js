(function() {
    'use strict';

    angular
        .module('module')
        .service('service', service);

    service.$inject = ['$http', '$q'];

    /* @ngInject */
    function service($http, $q) {
        this.login = postElements;
        this.peticion = getElements;

        function getElements(url, token){
          var defer = $q.defer();

          var headers = {
            'Authorization': 'Token '+token
          }

          $http.get('http://localhost:8000/'+url, {headers:headers}).then(function(res){
            defer.resolve(res)
          }, function(err){
            defer.reject(err);
          })
          return defer.promise;
        };

        function postElements(user, pwd) {
          var defer = $q.defer();

          var url = "http://localhost:8000/rest-auth/login/";

          var datos = {
            email: user,
            password: pwd
          };

          $http.post(url, datos).then(function(res){
            defer.resolve(res)
          }, function(err){
            defer.reject(err);
          })
          return defer.promise;
        };
    }
})();
